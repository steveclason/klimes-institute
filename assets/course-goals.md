Course Goals:

1. Recognize essential elements of end of life care.

  Objectives:
    Identify and explain four domains of end of life care:
      Physical Comfort
      Mental and Emotional Needs
      Spiritual Needs
      Practical Tasks

2. Identify legal and structural frameworks for care.

  Objectives: 
    Identify key elements of legal and structural frameworks for end of life care
      Advance directive
      Do Not Resuscitate Directive
      Palliative Care
      Hospice Care

3. Be prepared to work in a care team to address a patient’s needs.

  Objectives: 
    Identify key elements of end of life care teams and explain how they can work together on a unique care plan
      Nurses
      Doctors and other medical professionals
      Family
      Community and other support