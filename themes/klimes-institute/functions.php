<?php
/**
 * For Klimes Institute, for testing.
 */

// Require shortcodes.
require ( dirname(__FILE__)  . '/shortcodes.php' );
require ( dirname(__FILE__)  . '/sensei/sensei-functions.php' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {
	wp_enqueue_style( 'klimes-institute-theme-css', get_stylesheet_directory_uri() . '/style.css', array( 'astra-theme-css' ), wp_get_theme() -> get( 'Version' ), 'all' );
}
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );