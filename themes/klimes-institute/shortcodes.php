<?php
/**
 * 
 */

 /**
  * Display a serial number on the printed course certificate.
  */

if ( !function_exists ( 'ce_cert_serialize_function' )) {
  function ce_cert_serialize_function( $atts ) {
    $a = shortcode_atts( array(

      // default attributes
      // 'callout_title' => 'No Title'
    ), $atts );

    // TODO Concatenate userid abnd courseid?
    $user_id = get_current_user_id();
    $cert_id = get_the_id();

    // $html = '<p>ABC123</p>';
    $html = 'Certificate No. ' . $user_id . '-' . $cert_id;

    return $html;

  } // shortcode function.
} // function_exists.

add_shortcode( 'cert_serialize', 'ce_cert_serialize_function' );